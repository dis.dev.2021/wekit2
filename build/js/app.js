'use strict';

document.addEventListener("DOMContentLoaded", function() {

    // SVG IE11 support
    svg4everybody();

    const priceMobileSlider = new Swiper('.price-mobile-slider', {
        loop: false,
    });

    priceMobileSlider.on('slideChange', function () {
        let slideNav = '.price-header__item--0' + (priceMobileSlider.realIndex + 1);
        console.log(slideNav);
        $('.price-header').find('.price-header__item').removeClass('active');
        $(slideNav).addClass('active');
    });

    $('.price-header__item').on("click", function (e) {
        e.preventDefault();
        let $this = $(this);
        let slide = $this.attr('data-slide');
        $this.closest('.price-header').find('.price-header__item').removeClass('active');
        $this.addClass('active');
        priceMobileSlider.slideTo(slide, 400);
    });

    function stickySidenav() {
        const label = document.querySelector('.root');
        const stickyLabel = document.querySelector('#box-01');
        let  $h = $(stickyLabel).offset().top - $(window).height() + 100;

        $(window).scroll(function () {
            if ($(window).scrollTop() > $h) {
                $('.sidenav').addClass('sidenav--visible');
            } else {
                $('.sidenav').removeClass('sidenav--visible');
            }
        });

        $('.sidenav__link').on("click", function () {
            let $this = $(this);
            $('.sidenav__link').removeClass('active');
            $this.addClass('active');
        });


        let sections = $('.box'),
            nav = $('.sidenav__menu'),
            nav_height = nav.outerHeight();
        $(window).on('scroll', function () {
            $('.sidenav__menu a').removeClass('active');
            let cur_pos = $(this).scrollTop();
            sections.each(function() {
                let top = $(this).offset().top - nav_height - 180,
                    bottom = top + $(this).outerHeight();
                if (cur_pos >= top && cur_pos <= bottom) {
                    nav.find('a').removeClass('active');
                    sections.removeClass('active');
                    $(this).addClass('active');
                    nav.find('a[href="#'+$(this).attr('id')+'"]').addClass('active');
                }
            });
        });
        nav.find('a').on('click', function () {
            let $el = $(this),
                id = $el.attr('href');
            $('html, body').animate({
                scrollTop: $(id).offset().top - nav_height
            }, 800);
            return false;
        });
    }
    stickySidenav();


    let block_show = null;

    function scrollTracking(){
        let sideNav = $('.sidenav');
        let wt = $(window).scrollTop();
        let wh = $(window).height();
        let et = $('.foot-block').offset().top;
        let eh = ($('.foot-block').outerHeight() + 100);

        if (wt + wh >= et && wt + wh - eh * 2 <= et + (wh - eh)){
            if (block_show == null || block_show == false) {
                let footOffset = $('.foot-block').offset().top - sideNav.height();
                console.log(footOffset);
                sideNav.addClass('sidenav--scroll');
                sideNav.css('top', footOffset);
            }
            block_show = true;
        } else {
            if (block_show == null || block_show == true) {
                sideNav.removeClass('sidenav--scroll');
                sideNav.removeAttr('style');
            }
            block_show = false;
        }
    }

    $(window).scroll(function(){
        scrollTracking();
    });

    $(document).ready(function(){
        scrollTracking();
    });

});